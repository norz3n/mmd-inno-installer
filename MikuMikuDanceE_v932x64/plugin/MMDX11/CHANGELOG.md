# CHANGELOG

## [0.1.0] - 2020-1-3
### Chagned:
- Fixed many bugs.
- Removed Effects for Direct3D 11.
- Support ComputeShader.
- Support RWTexture2D and RWStructuredBuffer.
- Remove upper limit of multi sample count.
- Improvement compile error message.

## [0.0.5] - 2019-11-27
### Chagned:
- Fixed shared texture parameter not setting texture.
- Fixed failed to parse static function.

## [0.0.4] - 2019-11-23
### Chagned:
- Change the upper limit of multi sample count to 4.

### Fixed:
- Fixed a problem that don't created mipmap.
- Modified to clear the depth buffer to the minimum number of times.

## [0.0.3] - 2019-11-22
### Fixed:
- Fixed a problem that created mipmap every time.

## [0.0.2] - 2019-11-22
### Fixed:
- Fixed crash when loading size 1 texture.
- Fixed deleting an object release the texture and a possible crash.
- Fixed reference the VIEWPORTPIXELSIZE variable in a position where it has not yet been declared.
- Fixed the join order is incorrect when parsing a cast expression.
- Fixed the join order is incorrect when generating a cast expression.
- Fixed clipping transparent material does not work.
- Fixed not being able to convert if there is an array when compatible with VS and PS.

## [0.0.1] - 2019-11-11
- Initial release.