﻿＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊

  ソフト名： Effekseer for MMD
    作成者： おいがみ
  転載条件： Effekseer for MMD 本体の転載は、非商用に限り連絡不要です。
  使用条件： Effekseer for MMDを使用することや、使用して作成した作品については、
             商用、非商用問わず制限はありません。
             公開されてるエフェクトを使用する場合はその使用条件に従ってください。
　  連絡先： twitter： https://twitter.com/oigami013
              github： https://github.com/oigami/EffekseerForMMD/issues
開発サイト： github： https://github.com/oigami/EffekseerForMMD/

    その他： MMEが同梱されているのでMMEffect.txtもお読みください
    
＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊


・インストール方法
https://github.com/oigami/MMDUtility/wiki/how_to_install

・「EffekseerForMMDの備忘録」 / rn9dfj3 さんの作品 - ニコニ立体 
  http://3d.nicovideo.jp/works/td28150 

・Home ・ oigami/EffekseerForMMD Wiki 
  https://github.com/oigami/EffekseerForMMD/wiki 

・更新履歴
ver 0.25
  Effekseer 1.30に更新
  StopRootに対応
Ver 0.24
  Effekseer 1.30(Beta2.2-17/07/04)に更新
  efkのモデル名が空になっていた問題を修正
Ver 0.23
  プラグインを使用した場合にMMEで材質の名前が正しくならない問題を修正
Ver 0.22
  Effekseer 1.30(Beta-17/05/14) に対応